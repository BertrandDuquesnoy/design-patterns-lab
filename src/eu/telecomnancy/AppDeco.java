package eu.telecomnancy;

import eu.telecomnancy.sensor.DecorateurFarhenheit;
import eu.telecomnancy.sensor.DecorateurTemperatureSensor;
import eu.telecomnancy.sensor.TemperatureSensor;
import eu.telecomnancy.ui.ConsoleUI;

public class AppDeco {

    public static void main(String[] args) {
        TemperatureSensor sensor = new TemperatureSensor();
        DecorateurTemperatureSensor s = new DecorateurFarhenheit(sensor);
        new ConsoleUI(s);
    }
}	