package eu.telecomnancy.sensor;

import java.util.Random;


public class EtatOnWS implements EtatWindSensor {
	AutomateWindSensor a;
	
	public EtatOnWS(AutomateWindSensor b){
		this.a=b;
	}
	@Override
	public void on() {
		
		
	}

	@Override
	public void off() {
		a.changerEtat(new EtatOffWS(a));
		
	}

	@Override
	public boolean getStatus() {
		
		return true;
	}

	@Override
	public String update() {
		
		return String.valueOf(this.getValue())+"km/h";
		
	}

	@Override
	public double getValue() {
		return (new Random()).nextDouble() * 100 ;
	}

	
	

	

}
