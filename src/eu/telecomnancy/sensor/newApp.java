package eu.telecomnancy.sensor;

import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.LegacyTemperatureSensor;

public class newApp implements ISensor {
	private LegacyTemperatureSensor sensor;
	
	public newApp(){
		sensor = new LegacyTemperatureSensor();
		
	}

	public void on() {
		if(!sensor.getStatus()){
			sensor.onOff();
		}
	}

	public void off() {
		if(sensor.getStatus()){
			sensor.onOff();
		}
	}

	public boolean getStatus() {
		
		return false;
	}

	public void update() throws SensorNotActivatedException {

	}

	@Override
	public double getValue() throws SensorNotActivatedException {

		return sensor.getTemperature();
	}

}	