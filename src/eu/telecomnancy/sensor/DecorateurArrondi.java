package eu.telecomnancy.sensor;

public class DecorateurArrondi extends DecorateurTemperatureSensor {

	public DecorateurArrondi(TemperatureSensor s){
		sensor = s;
	}

	@Override
	public void on() {
		sensor.on();
		
	}

	@Override
	public void off() {
		sensor.off();
		
	}

	@Override
	public double getValue() throws SensorNotActivatedException {
		return Math.rint(sensor.getValue());
	}

	@Override
	public void update() throws SensorNotActivatedException {
		sensor.update();
	}	
}