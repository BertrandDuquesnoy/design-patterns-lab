package eu.telecomnancy.sensor;

import eu.telecomnancy.ui.Observer;

public interface Observable {
	public void addObserver(Observer obs);
	public void removeObserver();
	public void notifyObserver(String str);

}
