package eu.telecomnancy.sensor;

public abstract class DecorateurTemperatureSensor extends TemperatureSensor {
	protected TemperatureSensor sensor;
	
	public abstract void on();
	public abstract void off();
	public abstract double getValue()throws SensorNotActivatedException;
	public abstract void update() throws SensorNotActivatedException;
}