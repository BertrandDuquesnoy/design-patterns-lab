package eu.telecomnancy.sensor;

import eu.telecomnancy.sensor.TemperatureSensor;

public class ConcreteCommandOff implements Commands{
	private TemperatureSensor sensor;
	
	public ConcreteCommandOff(TemperatureSensor sensor) {
		this.sensor=sensor;
	}
	@Override
	public void execute() {
		sensor.off();
		System.out.println("Eteint");
	}
	
}