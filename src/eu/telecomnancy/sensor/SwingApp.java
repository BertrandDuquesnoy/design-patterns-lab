package eu.telecomnancy.sensor;

import eu.telecomnancy.ui.MainWindow;

public class SwingApp {

    public static void main(String[] args) throws SensorNotActivatedException{
        ISensor sensor = new TemperatureSensor();
        new MainWindow(sensor);
   
    }
}
