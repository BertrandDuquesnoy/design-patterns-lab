package eu.telecomnancy.sensor;



public class AutomateWindSensor {
	private EtatWindSensor etat;
	
	public AutomateWindSensor(){
		this.etat=new EtatOffWS(this);
	}
	
	public void changerEtat(EtatWindSensor e){
		this.etat=e;
	}
	
	public EtatWindSensor getEtat(){
		return etat;
	}
	
	
}
