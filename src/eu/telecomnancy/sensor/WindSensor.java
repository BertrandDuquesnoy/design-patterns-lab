package eu.telecomnancy.sensor;
import java.util.Random;

public class WindSensor extends AbstractISensor {
	AutomateWindSensor contexte=new AutomateWindSensor();
	@Override
	public void on() {
		this.contexte.getEtat().on();
		
	}

	@Override
	public void off() {
		this.contexte.getEtat().off();
		
	}

	@Override
	public boolean getStatus() {
		return this.contexte.getEtat().getStatus();
	}
	
	public void update(){
       
        this.notifyObserver(this.contexte.getEtat().update());
        
    }
    

	@Override
	public double getValue() {
		return this.contexte.getEtat().getValue();
	}

}
