package eu.telecomnancy.sensor;

import java.util.Random;

public class LegacyTemperatureSensor implements ISensor{
    private boolean state = false;
    private double start = -20;
    private double end = 100;
    private double value;
    private AcquiringThread worker = null;

    /**
     * Enable/disable the sensor.
     *
     * @return the current sensor status.
     */
    public synchronized boolean onOff() {
        this.state = ! this.state;
        if (this.state) {
            this.worker = new AcquiringThread();
            this.worker.start();
        } else {
            this.worker.disable();
            this.worker = null;
        }

        return this.state;
    }

    /**
     * Get the status (enabled/disabled) of the sensor.
     *
     * @return the current sensor's status.
     */
    public boolean getStatus() {
        return this.state;
    }

    /**
     * Get the current temperature.
     *
     * @return the latest recorded temperature.
     */
    public double getTemperature() {
        return this.value;
    }

    private class AcquiringThread extends Thread {
        private boolean active = true;

        /**
         * Stop the acquiring thread. There is no way to start it again ;)
         */
        public void disable() {
            this.active = false;
        }

        @Override
        public void run() {
            while (this.active) {
                double random = (new Random()).nextDouble();
                value = start + (random * (end - start));
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

	@Override
	public void on() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void off() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void update() throws SensorNotActivatedException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public double getValue() throws SensorNotActivatedException {
		// TODO Auto-generated method stub
		//this.value = this.sensor.getTemperature();
		return 0;
	}

}
