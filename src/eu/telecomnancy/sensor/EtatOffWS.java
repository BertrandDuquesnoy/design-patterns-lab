package eu.telecomnancy.sensor;


public class EtatOffWS implements EtatWindSensor {
	
	AutomateWindSensor a;
	
	public EtatOffWS(AutomateWindSensor b){
		this.a=b;
	}
	
	@Override
	public void on() {
		System.out.println("on");
		a.changerEtat(new EtatOnWS(a));
		
	}

	@Override
	public void off() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean getStatus() {
		
		return false;
	}

	@Override
	public String update() {
		
		return "Capteur éteint !";
		
	}

	@Override
	public double getValue() {
		// TODO Auto-generated method stub
		return 0;
	}

	
}
