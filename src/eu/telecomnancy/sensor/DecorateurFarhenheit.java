package eu.telecomnancy.sensor;

public class DecorateurFarhenheit extends DecorateurTemperatureSensor {

	public DecorateurFarhenheit(TemperatureSensor s){
		sensor = s;
	}

	@Override
	public void on() {
		sensor.on();
		
	}

	@Override
	public void off() {
		sensor.off();
		
	}

	@Override
	public double getValue() throws SensorNotActivatedException {
		return sensor.getValue()*1.8+32;
	}	
	
	public void update() throws SensorNotActivatedException {
		sensor.update();
	}	
}