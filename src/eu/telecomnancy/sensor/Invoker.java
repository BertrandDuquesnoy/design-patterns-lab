package eu.telecomnancy.sensor;

import java.util.List;
import java.util.ArrayList;

public class Invoker{
	private List<Commands> file = new ArrayList<Commands>();
	 
	   public Invoker() {
	   }
	 
	   public void storeAndExecute(Commands cmd) {
	      this.file.add(cmd);
	      cmd.execute();        
	   }
}