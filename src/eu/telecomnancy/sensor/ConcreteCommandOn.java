package eu.telecomnancy.sensor;

import eu.telecomnancy.sensor.TemperatureSensor;

public class ConcreteCommandOn implements Commands{
	private TemperatureSensor sensor;

	public ConcreteCommandOn(TemperatureSensor sensor) {
		this.sensor=sensor;
	}
	@Override
	public void execute() {
		sensor.on();
		System.out.println("Allumé");
	}
	
}