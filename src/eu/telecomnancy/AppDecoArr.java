package eu.telecomnancy;

import eu.telecomnancy.sensor.DecorateurArrondi;
import eu.telecomnancy.sensor.DecorateurFarhenheit;
import eu.telecomnancy.sensor.DecorateurTemperatureSensor;
import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.TemperatureSensor;
import eu.telecomnancy.ui.ConsoleUI;

public class AppDecoArr {

    public static void main(String[] args) {
        TemperatureSensor sensor = new TemperatureSensor();
        DecorateurTemperatureSensor s = new DecorateurArrondi(sensor);
        new ConsoleUI(s);
    }
}	