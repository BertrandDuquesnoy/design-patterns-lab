package eu.telecomnancy;

//import com.sun.java.util.jar.pack.Instruction.Switch; import non visible


import java.io.BufferedReader;
import java.io.InputStreamReader;

import eu.telecomnancy.sensor.TemperatureSensor;
import eu.telecomnancy.sensor.ConcreteCommandOn;
import eu.telecomnancy.sensor.ConcreteCommandOff;
import eu.telecomnancy.sensor.Invoker;

public class AppCommands {
    public static void main(String[] args) {
        TemperatureSensor sensor = new TemperatureSensor();
        ConcreteCommandOn ccon = new ConcreteCommandOn (sensor);
        ConcreteCommandOff ccoff = new ConcreteCommandOff (sensor);
        Invoker invoker = new Invoker();
        
        invoker.storeAndExecute(ccon); 
        invoker.storeAndExecute(ccoff);
        
        Invoker i = new Invoker();
        
        System.out.println("Entrez l'argument ON ou OFF pour allumer ou éteindre le capteur : ");
        
        try {
        	BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));
        	String s = bufferRead.readLine();
        	
           if (s.equalsIgnoreCase("ON")) {
              i.storeAndExecute(ccon);
              System.exit(0);
           }
           if (s.equalsIgnoreCase("OFF")) {
              i.storeAndExecute(ccoff);
              System.exit(0);
           }
           System.out.println("Argument \"ON\" or \"OFF\" is required.");
        } catch (Exception e) {
           System.out.println("Argument required.");
        }
        
    }
}